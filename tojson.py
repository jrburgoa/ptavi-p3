import smil
import sys
import json

class SMILJSON(smil.SMIL):
    def json(self):
        lista = []
        file = smil.SMIL(self.path)

        for element in file.elements():
            element_obj = ElementJSON(element.name(), element.attrs(), file.elements())
            lista.append(element_obj.dict())

        json_string = json.dumps(lista, indent=1) #generalizar a formato json
        return json_string


class ElementJSON(smil.Element):

    def dict(self):
        json_dict = {'name': self.name(), 'attrs': self.diccionario }
        return json_dict


def main (path = None):
    if path == None:
        print("Usage: python3 elements.py <file>" )
    else:
        object = SMILJSON(path)
        object.json()
        print(object.json())


if __name__ == "__main__":
    main(sys.argv[1])
